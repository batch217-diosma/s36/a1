// setup dependencies/modules

const express = require("express");

const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js");

// server setup

const app = express();

const port = 3001;

// middlewares

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.use("/tasks", taskRoute);
//localhost:3001/tasks/viewTasks - get
//localhost:3001/tasks/addNewTask - post


// DB connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.8qw2w8u.mongodb.net/B217_to_do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

app.listen(port , () => console.log(`Now listening to port ${port}`));

